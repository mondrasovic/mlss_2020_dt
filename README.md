# Machine Learning Summer School (MLSS) 2020

A personal project as part of the Machine Learning Summer School (MLSS) 2020 aimed at decision trees.

## Resources

[Gradient Boosting](https://www.gormanalysis.com/blog/gradient-boosting-explained/)
[MLSS 2020 Program Schedule](https://docs.google.com/document/d/1JrgcevFYKfqInhSKufDsfz9uFz3xkjrTAiK2zyrtUbU/edit)

## Datasets

